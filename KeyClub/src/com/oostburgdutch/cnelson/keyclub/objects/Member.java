package com.oostburgdutch.cnelson.keyclub.objects;

import com.oostburgdutch.cnelson.keyclub.enums.Grade;
import com.oostburgdutch.cnelson.keyclub.enums.Role;

public class Member {
	
	private String fName;
	private String lName;
	private String pName;
	private String email;
	private String password;
	private Grade grade;
	private Role role;
	
	public Member(String fName, String lName, String pName, String email, Grade grade, Role role, String password){
		this.fName = fName;
		this.lName = lName;
		this.pName = pName;
		this.email = email;
		this.grade = grade;
		this.role = role;
		this.password = password;
	}
	
	public Member(String fName, String lName, String email, Grade grade, Role role, String password){
		this.fName = fName;
		this.lName = lName;
		this.pName = fName;
		this.email = email;
		this.grade = grade;
		this.role = role;
		this.password = password;
	}

	public String getFirstName() {
		return fName;
	}

	public void setFirstName(String fName) {
		this.fName = fName;
	}
	
	public String getLastName() {
		return lName;
	}

	public void setLastName(String lName) {
		this.lName = lName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public String getPrefferedName() {
		return pName;
	}

	public void setPrefferedName(String pName) {
		this.pName = pName;
	}
	
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public String toString(){
		return lName+", "+fName+"\n - Preffered Name: "+pName+"\n - Grade: "+grade.toString()+"\n - Role: "+role.toString();
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getPassword(){
		return password;
	}
}
