package com.oostburgdutch.cnelson.keyclub.objects.forms;

import java.util.ArrayList;

public interface Form {
	public ArrayList<String> generateForm();
}
