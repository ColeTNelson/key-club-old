package com.oostburgdutch.cnelson.keyclub.objects.forms;

import java.util.ArrayList;

import com.oostburgdutch.cnelson.keyclub.objects.Member;

public class ReminderEmail implements Form {
	private String eventName;
	private String date;
	private String time;
	private String address;
	private String pName;
	
	public ReminderEmail(String pName, String eventName, String date, String time, String address) {
		this.eventName = eventName;
		this.date = date;
		this.time = time;
		this.address = address;
		this.pName = pName;
	}

	public ArrayList<String> generateForm(){
		ArrayList<String> array = new ArrayList<String>();
		array.add("Hey "+pName+",");
		array.add("");
		array.add("Just a quick reminder that you are signed up to help out at "
				+ eventName + " on "+ date +" from " + time + ". The address is "+ address + ".");
		array.add("");
		array.add("If you cannot make it, talk to an officer or advisor ASAP.");
		array.add("");
		array.add("Thanks for volunteering!");
		array.add("");
		array.add("");
		array.add("This is a semi-automated message. If you believe this was delivered to the wrong address or contains incorrect information please bring it up at the next meeting.");
		return array;
	}
	
	public ArrayList<String> generateForm(ArrayList<Member> volunteers){
		ArrayList<String> array = new ArrayList<String>();
		array.add("Hey "+pName+",");
		array.add("");
		array.add("Just a quick reminder that you are signed up to help out at "
				+ eventName + " on "+ date +" from " + time + ". The address is "+ address + ".");
		array.add("");
		array.add("If you cannot make it, talk to an officer or advisor ASAP.");
		array.add("");
		array.add("Thanks for volunteering!");
		array.add("");
		array.add("If you need to hitch a ride, the following members are attending as well:");
		for(Member m : volunteers){
			array.add(" - "+m.getPrefferedName()+" "+m.getLastName());
		}
		array.add("");
		array.add("");
		array.add("This is an automated message. If you believe this was delivered to the wrong address or contains incorrect information please bring it up at the next meeting.");
		return array;
	}
}