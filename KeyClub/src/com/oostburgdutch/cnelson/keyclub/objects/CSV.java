package com.oostburgdutch.cnelson.keyclub.objects;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CSV {
	private ArrayList<ArrayList<String>> csv = new ArrayList<ArrayList<String>>();
	private int rows;
	
	public CSV(String path) throws Exception{
		load(new File(path));
	}
	
	public CSV(File f) throws Exception{
		load(f);
	}
	
	public void load(File f) throws Exception{
		String line;
		ArrayList<String> rowArray = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			while ((line = br.readLine()) != null) {
				rowArray.add(line);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new Exception();
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception();
		}
		rows = rowArray.size();
		for(String s : rowArray){
			csv.add(constructRowArray(s.split(",")));
		}
	}
	
	private ArrayList<String> constructRowArray(String[] split){
		boolean stack = false;
		ArrayList<String> array = new ArrayList<String>();
		String tempString = "";
		for(String s : split){
			if(s.contains("\"")){
				if(stack){
					stack = false;
					tempString = tempString.concat(s.replaceAll("\"", ""));
					array.add(tempString);
					tempString = new String();
				}else{
					stack = true;
					tempString = tempString.concat(s.replaceAll("\"", "")+",");
				}
			}else if(stack){
				tempString = tempString.concat(s);
			}else{
				array.add(s);
			}
		}
		return array;
	}
	
	public String getCell(Character column, Integer row){
		return getRow(row).get(column-'a');
	}
	
	public String getCell(Integer column, Integer row){
		return getRow(row).get(column-1);
	}
	
	public ArrayList<String> getRow(int num){
		return csv.get(num-1);
	}
	
	public ArrayList<String> getColumn(int num){
		ArrayList<String> column = new ArrayList<String>();
		for(ArrayList<String> row : csv){
			if(row.size()>=num){
				column.add(row.get(num-1));
			}else{
				column.add(null);
			}
		}
		return column;
	}
	
	public ArrayList<String> getColumn(char c){
		int num = Character.getNumericValue(c);
		ArrayList<String> column = new ArrayList<String>();
		for(ArrayList<String> row : csv){
			if(row.size()>=num){
				column.add(row.get(num-1));
			}else{
				column.add(null);
			}
		}
		return column;
	}
	
	public int getTotalRows(){
		return this.rows;
	}
}