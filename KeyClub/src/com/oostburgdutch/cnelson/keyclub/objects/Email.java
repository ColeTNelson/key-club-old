package com.oostburgdutch.cnelson.keyclub.objects;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {
	
	String address;
	String password;
	String subject;
	ArrayList<String> body;
	
	public Email(String address, String password, String subject, ArrayList<String> body){
		this.address = address;
		this.password = password;
		this.subject = subject;
		this.body = body;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public ArrayList<String> getBody() {
		return body;
	}

	public void setBody(ArrayList<String> body) {
		this.body = body;
	}
	
	public void send(){    
		final String username = "oostburgkeyclub@gmail.com";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("oostburgkeyclub@gmail.com"));
			message.setRecipients(RecipientType.TO,
				InternetAddress.parse(this.address));
			message.setSubject(this.subject);
			String text = "";
			for(String line : this.body){
				text=text.concat("\n"+line);
			}
			message.setText(text);
			Transport.send(message);

		} catch (MessagingException exception) {
			throw new RuntimeException(exception);
		}
	}
}
