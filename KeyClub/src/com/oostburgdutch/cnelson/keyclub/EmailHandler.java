package com.oostburgdutch.cnelson.keyclub;

import java.util.ArrayList;

import com.oostburgdutch.cnelson.keyclub.objects.Email;

public class EmailHandler implements Runnable{
	
	ArrayList<Email> emails;
	
	public EmailHandler(ArrayList<Email> emails){
		this.emails = emails;
	}
	
	@Override
	public void run(){
		for(Email e : emails){
			e.send();
		}
	}
}
