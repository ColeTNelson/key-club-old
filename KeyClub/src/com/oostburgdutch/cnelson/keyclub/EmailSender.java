package com.oostburgdutch.cnelson.keyclub;

import java.util.*;
import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.*;

import com.oostburgdutch.cnelson.keyclub.objects.Email;

public class EmailSender{
	
	public static void send(Email e){    
		final String username = "oostburgkeyclub@gmail.com";
		final String password = e.getPassword();

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("oostburgkeyclub@gmail.com"));
			message.setRecipients(RecipientType.TO,
				InternetAddress.parse(e.getAddress()));
			message.setSubject(e.getSubject());
			String text = "";
			for(String line : e.getBody()){
				text=text.concat("\n"+line);
			}
			message.setText(text);
			Transport.send(message);

		} catch (MessagingException exception) {
			throw new RuntimeException(exception);
		}
	}
}
