package com.oostburgdutch.cnelson.keyclub;

import java.util.ArrayList;

import com.oostburgdutch.cnelson.keyclub.objects.Member;

public class MemberIdentifier {
	public static Member getMember(String s){
		ArrayList<Member> matches = getMembers(s);
		if(matches.isEmpty() || matches.size() > 1){
			return null;
		}
		return matches.get(0);
	}
	public static ArrayList<Member> getMembers(String s){
		s = s.toLowerCase();
		ArrayList<Member> matches = new ArrayList<Member>();
		if(s.contains(" ")){
			String[] split = s.split(" ");
			for(Member m : Database.getMembers()){
				if((m.getFirstName().toLowerCase().startsWith(split[0]) || m.getPrefferedName().toLowerCase().startsWith(split[0])) && m.getLastName().toLowerCase().startsWith(split[1])){
					matches.add(m);
				}
			}
			if(matches.isEmpty()){
				for(Member m : Database.getMembers()){
					if((m.getFirstName().toLowerCase().startsWith(split[1]) || m.getPrefferedName().toLowerCase().startsWith(split[1])) && m.getLastName().toLowerCase().startsWith(split[0])){
						matches.add(m);
					}
				}
			}
		}else{
			for(Member m : Database.getMembers()){
				if(m.getFirstName().toLowerCase().startsWith(s) || m.getPrefferedName().toLowerCase().startsWith(s)){
					matches.add(m);
				}
			}
			if(matches.isEmpty()){
				for(Member m : Database.getMembers()){
					if(m.getLastName().toLowerCase().startsWith(s)){
						matches.add(m);
					}
				}	
			}
		}
		return matches;
	}
}
