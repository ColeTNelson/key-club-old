package com.oostburgdutch.cnelson.keyclub.commands;

import java.io.File;
import java.util.ArrayList;

import com.oostburgdutch.cnelson.keyclub.Database;
import com.oostburgdutch.cnelson.keyclub.KeyClub;
import com.oostburgdutch.cnelson.keyclub.MemberIdentifier;
import com.oostburgdutch.cnelson.keyclub.ThreadHandler;
import com.oostburgdutch.cnelson.keyclub.Util;
import com.oostburgdutch.cnelson.keyclub.objects.Email;
import com.oostburgdutch.cnelson.keyclub.objects.Member;
import com.oostburgdutch.cnelson.keyclub.objects.TXT;

public class CommandCustomReminder {
	public static void execute(boolean splash){
		if(splash){
			System.out.println();
			System.out.println(" == OOSTBURG KEY CLUB - TECHNOLOGY PANEL == ");
			System.out.println("      --------------------------------      ");
			System.out.println("           Custom Remind Feature            ");
		}
		System.out.println();
		System.out.println(" Type in the file name or 'RETURN':");
		String name = Util.getScanner().nextLine();
		if(name.equalsIgnoreCase("") || name.equalsIgnoreCase("RETURN")){
			KeyClub.splash(true);
		}
		TXT txt;
		File f = new File(Util.getRoot()+"\\Custom Remind Forms\\"+name);
		if(f.exists()){
			try {
				txt = new TXT(f);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}else{
			f = new File(Util.getRoot()+"\\Custom Remind Forms\\"+name+".txt");
			if(f.exists()){
				try {
					txt = new TXT(f);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
			}else{
				KeyClub.splash(true);
				return;
			}
		}
		System.out.println();
		System.out.println(" Form found '"+f.getName()+"'");
		System.out.println(" Please provide a password for oostburgkeyclub@gmail.com:");
		String password = Util.getScanner().nextLine();
		System.out.println();
		Util.sendSpacer();
		System.out.println("Type in people to e-mail. When done type 'DONE'. Type 'RETURN' to cancel.");
		System.out.println("Type 'ALL' for all members.");
		String input;
		ArrayList<Member> names = new ArrayList<Member>();
		while(!(input = Util.getScanner().nextLine()).equalsIgnoreCase("RETURN") && !input.equalsIgnoreCase("DONE") && !input.equalsIgnoreCase("ALL")){
			Member m = MemberIdentifier.getMember(input);
			if(m == null){
				System.out.println();
				System.out.println(input+" is not a registered member!");
			}else{
				names.add(m);
				System.out.println();
				System.out.println(m.getFirstName()+" " + m.getLastName() +" added!");
			}
		}
		if(input.equalsIgnoreCase("RETURN")){
			KeyClub.splash(true);
			return;
		}
		if(input.equalsIgnoreCase("ALL")){
			names.clear();
			names.addAll(Database.getMembers());
		}
		System.out.println();
		System.out.println(" Sending...");
		ArrayList<Email> emails = new ArrayList<Email>();
		for(Member m : names){
			emails.add(new Email(m.getEmail(), password,"Key Club Notification",txt.generateForm()));
		}
		ThreadHandler.sendEmails(emails);
		System.out.println();
		System.out.println(" Form sent to the following person(s):");
		for(Member m : names){
			System.out.println(" - "+m.getEmail());
		}
		System.out.println();
		System.out.println(" Press 'return' to continue...");
		Util.getScanner().nextLine();
		KeyClub.splash(true);
	}
}
