package com.oostburgdutch.cnelson.keyclub.commands;

import java.io.File;
import java.util.ArrayList;

import com.oostburgdutch.cnelson.keyclub.EmailSender;
import com.oostburgdutch.cnelson.keyclub.KeyClub;
import com.oostburgdutch.cnelson.keyclub.MemberIdentifier;
import com.oostburgdutch.cnelson.keyclub.ThreadHandler;
import com.oostburgdutch.cnelson.keyclub.Util;
import com.oostburgdutch.cnelson.keyclub.objects.CSV;
import com.oostburgdutch.cnelson.keyclub.objects.Email;
import com.oostburgdutch.cnelson.keyclub.objects.Member;
import com.oostburgdutch.cnelson.keyclub.objects.forms.ReminderEmail;

public class CommandReminder {
	public static void execute(boolean splash){
		if(splash){
			System.out.println();
			System.out.println(" == OOSTBURG KEY CLUB - TECHNOLOGY PANEL == ");
			System.out.println("      --------------------------------      ");
			System.out.println("                Remind Feature              ");
		}
		System.out.println();
		System.out.println(" Type in the file name or 'RETURN':");
		String name = Util.getScanner().nextLine();
		if(name.equalsIgnoreCase("") || name.equalsIgnoreCase("RETURN")){
			KeyClub.splash(true);
		}
		CSV csv;
		File f = new File(Util.getRoot()+"\\Remind Forms\\"+name);
		if(f.exists()){
			try {
				csv = new CSV(f);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}else{
			f = new File(Util.getRoot()+"\\Remind Forms\\"+name+".csv");
			if(f.exists()){
				try {
					csv = new CSV(f);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
			}else{
				KeyClub.splash(true);
				return;
			}
		}
		System.out.println();
		System.out.println(" Form found '"+f.getName()+"'");
		System.out.println(" Please provide a password for oostburgkeyclub@gmail.com:");
		String password = Util.getScanner().nextLine();
		System.out.println();
		System.out.println(" Sending...");
		
		String address = csv.getCell('b', 4);
		String eName = csv.getCell('b', 3);
		ArrayList<String> errors = new ArrayList<String>();
		ArrayList<String> successes = new ArrayList<String>();
		ArrayList<Email> emails = new ArrayList<Email>();
		ArrayList<Member> members = new ArrayList<Member>();
		String[] split = csv.getCell('b', 5).split(",");
		String date = split[0];
		String time = split[1];
		ArrayList<String> column = csv.getColumn(2);
		for(int i=5;i<column.size();i++){
			String memberName = column.get(i);
			if(memberName!=null && !memberName.isEmpty()){
				Member m = MemberIdentifier.getMember(memberName);
				if(m==null){
					errors.add(memberName);
				}else{
					members.add(m);
				}
			}
		}
		for(Member m : members){
			ReminderEmail email = new ReminderEmail(m.getPrefferedName(), eName, date, time, address);
			emails.add(new Email(m.getEmail(), password,"Key Club - "+eName,email.generateForm(members)));
			successes.add(m.getEmail());
		}
		Member me = MemberIdentifier.getMember("Cole Nelson");
		if(!members.contains(me)){
			EmailSender.send(new Email(me.getEmail(), password,"Key Club . - "+eName, new ReminderEmail(me.getPrefferedName(), eName, date, time, address).generateForm(members)));
		}
		ThreadHandler.sendEmails(emails);
		System.out.println();
		if(errors.isEmpty()){
			System.out.println(" Operation completed successfully.");
		}else{
			System.out.println(" Failed to email the following members:");
			for(String person : errors){
				System.out.println(" - "+person);
			}
		}
		System.out.println();
		if(successes.isEmpty()){
			System.out.println(" Operation did not e-mail anyone.");
		}else{
			System.out.println(" E-mailed reminder to the following person(s):");
			for(String person : successes){
				System.out.println(" - "+person);
			}
		}
		System.out.println();
		System.out.println(" Type 'RETURN' to continue...");
		Util.getScanner().nextLine();
		KeyClub.splash(true);
	}
}
