package com.oostburgdutch.cnelson.keyclub.commands;

import com.oostburgdutch.cnelson.keyclub.Database;
import com.oostburgdutch.cnelson.keyclub.KeyClub;

public class CommandReload {
	public static void execute(){
		Database.init();
		System.out.println();
		System.out.println(" Database reloaded!");
		KeyClub.splash(false);
	}
}
