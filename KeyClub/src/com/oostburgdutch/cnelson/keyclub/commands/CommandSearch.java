package com.oostburgdutch.cnelson.keyclub.commands;

import java.util.ArrayList;

import com.oostburgdutch.cnelson.keyclub.KeyClub;
import com.oostburgdutch.cnelson.keyclub.MemberIdentifier;
import com.oostburgdutch.cnelson.keyclub.Util;
import com.oostburgdutch.cnelson.keyclub.objects.Member;

public class CommandSearch {
	
	public static void execute(boolean splash){
		if(splash){
			System.out.println();
			System.out.println(" == OOSTBURG KEY CLUB - TECHNOLOGY PANEL == ");
			System.out.println("      --------------------------------      ");
			System.out.println("                Search Feature              ");
		}
		System.out.println();
		System.out.println(" Type in a name or 'RETURN':");
		String name = Util.getScanner().nextLine();
		if(name.equalsIgnoreCase("") || name.equalsIgnoreCase("RETURN")){
			KeyClub.splash(true);
		}
		System.out.println();
		Member m = MemberIdentifier.getMember(name);
		if(m!=null){
			System.out.println("Match found!");
			System.out.println();
			Util.sendSpacer();
			System.out.println(m);
		}else{
			ArrayList<Member> members = MemberIdentifier.getMembers(name);
			if(members.isEmpty()){
				System.out.println("No matches found!");
			}else{
				System.out.println("Multiple matches found!");
				for(Member member : members){
					System.out.println();
					Util.sendSpacer();
					System.out.println(member);
				}
				System.out.println();
			}
		}
		Util.sendSpacer();
		CommandSearch.execute(false);
		return;
	}
}
