package com.oostburgdutch.cnelson.keyclub;

import java.util.Scanner;

public class Util {
	
	private static Scanner input = new Scanner(System.in);;
	private static String root = "C:\\Users\\Cole Nelson\\Desktop\\Key Club";
	
	public static void sendSpacer(){
		System.out.println("-------------------------");
	}
	
	public static Scanner getScanner(){
		return input;
	}
	
	public static String getRoot(){
		return root;
	}
}
