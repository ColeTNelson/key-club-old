package com.oostburgdutch.cnelson.keyclub;

import java.util.ArrayList;

import com.oostburgdutch.cnelson.keyclub.objects.Email;

public class ThreadHandler {
	public static void sendEmails(ArrayList<Email> emails){
		new Thread(new EmailHandler(emails)).start();
	}
}