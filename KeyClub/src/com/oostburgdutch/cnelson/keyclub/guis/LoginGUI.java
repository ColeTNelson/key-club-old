package com.oostburgdutch.cnelson.keyclub.guis;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginGUI {

	private JFrame frmKeyClub;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI window = new LoginGUI();
					window.frmKeyClub.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKeyClub = new JFrame();
		frmKeyClub.setTitle("Key Club - Login");
		frmKeyClub.setBounds(100, 100, 320, 203);
		frmKeyClub.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKeyClub.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Key Club - Login");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 408, 20);
		frmKeyClub.getContentPane().add(lblKeyClub);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(12, 46, 46, 16);
		frmKeyClub.getContentPane().add(lblEmail);
		
		textField = new JTextField();
		textField.setBounds(90, 43, 189, 22);
		frmKeyClub.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(12, 81, 66, 16);
		frmKeyClub.getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(90, 78, 189, 22);
		frmKeyClub.getContentPane().add(passwordField);
		
		JButton btnSugmit = new JButton("Submit");
		btnSugmit.setBounds(12, 110, 97, 25);
		frmKeyClub.getContentPane().add(btnSugmit);
		
		JButton btnForgotPassword = new JButton("Forgot Password?");
		btnForgotPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnForgotPassword.setBounds(135, 110, 144, 25);
		frmKeyClub.getContentPane().add(btnForgotPassword);
	}
}
