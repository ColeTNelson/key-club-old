package com.oostburgdutch.cnelson.keyclub.guis;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JToggleButton;
import javax.swing.JCheckBox;
import javax.swing.JSeparator;
import javax.swing.JScrollBar;

public class EventDetailsGUI {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	private JTextField textField_19;
	private JTextField textField_20;
	private JTextField textField_21;
	private JTextField textField_22;
	private JTextField textField_23;
	private JTextField textField_24;
	private JTextField textField_25;
	private JTextField textField_26;
	private JTextField textField_27;
	private JTextField textField_28;
	private JTextField textField_29;
	private JTextField textField_30;
	private JTextField textField_31;
	private JTextField textField_32;
	private JTextField textField_33;
	private JSeparator separator_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				
			}
		});*/
		try {
			EventDetailsGUI window = new EventDetailsGUI();
			window.frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the application.
	 */
	public EventDetailsGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setEnabled(false);
		frame.setBounds(100, 100, 450, 602);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCustomReminderEmail = new JLabel("Key Club - Event Details");
		lblCustomReminderEmail.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCustomReminderEmail.setBounds(12, 8, 205, 23);
		frame.getContentPane().add(lblCustomReminderEmail);
		
		JLabel lblEventName = new JLabel("Event Name:");
		lblEventName.setFont(new Font("Georgia", Font.BOLD, 14));
		lblEventName.setBounds(12, 52, 106, 16);
		frame.getContentPane().add(lblEventName);
		
		textField = new JTextField();
		textField.setBounds(114, 49, 190, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Georgia", Font.BOLD, 14));
		lblAddress.setBounds(12, 84, 106, 16);
		frame.getContentPane().add(lblAddress);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(83, 81, 190, 22);
		frame.getContentPane().add(textField_1);
		
		JLabel lblTime = new JLabel("Time:");
		lblTime.setFont(new Font("Georgia", Font.BOLD, 14));
		lblTime.setBounds(234, 119, 53, 16);
		frame.getContentPane().add(lblTime);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(284, 116, 106, 22);
		frame.getContentPane().add(textField_2);
		
		JLabel lblDate = new JLabel("Date:");
		lblDate.setFont(new Font("Georgia", Font.BOLD, 14));
		lblDate.setBounds(12, 119, 59, 16);
		frame.getContentPane().add(lblDate);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(58, 116, 159, 22);
		frame.getContentPane().add(textField_3);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("All Members Attending");
		chckbxNewCheckBox.setBounds(190, 170, 169, 25);
		frame.getContentPane().add(chckbxNewCheckBox);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 161, 397, 2);
		frame.getContentPane().add(separator);
		
		textField_4 = new JTextField();
		textField_4.setBounds(12, 208, 116, 22);
		frame.getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(157, 208, 116, 22);
		frame.getContentPane().add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(293, 208, 116, 22);
		frame.getContentPane().add(textField_6);
		
		JLabel lblAttendees = new JLabel("Attendees");
		lblAttendees.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		lblAttendees.setBounds(72, 172, 89, 16);
		frame.getContentPane().add(lblAttendees);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(12, 243, 116, 22);
		frame.getContentPane().add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(157, 243, 116, 22);
		frame.getContentPane().add(textField_8);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(293, 243, 116, 22);
		frame.getContentPane().add(textField_9);
		
		textField_10 = new JTextField();
		textField_10.setColumns(10);
		textField_10.setBounds(12, 279, 116, 22);
		frame.getContentPane().add(textField_10);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(157, 279, 116, 22);
		frame.getContentPane().add(textField_11);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(293, 279, 116, 22);
		frame.getContentPane().add(textField_12);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(12, 314, 116, 22);
		frame.getContentPane().add(textField_13);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		textField_14.setBounds(157, 314, 116, 22);
		frame.getContentPane().add(textField_14);
		
		textField_15 = new JTextField();
		textField_15.setColumns(10);
		textField_15.setBounds(293, 314, 116, 22);
		frame.getContentPane().add(textField_15);
		
		textField_16 = new JTextField();
		textField_16.setColumns(10);
		textField_16.setBounds(12, 349, 116, 22);
		frame.getContentPane().add(textField_16);
		
		textField_17 = new JTextField();
		textField_17.setColumns(10);
		textField_17.setBounds(157, 349, 116, 22);
		frame.getContentPane().add(textField_17);
		
		textField_18 = new JTextField();
		textField_18.setColumns(10);
		textField_18.setBounds(293, 349, 116, 22);
		frame.getContentPane().add(textField_18);
		
		textField_19 = new JTextField();
		textField_19.setColumns(10);
		textField_19.setBounds(12, 384, 116, 22);
		frame.getContentPane().add(textField_19);
		
		textField_20 = new JTextField();
		textField_20.setColumns(10);
		textField_20.setBounds(157, 384, 116, 22);
		frame.getContentPane().add(textField_20);
		
		textField_21 = new JTextField();
		textField_21.setColumns(10);
		textField_21.setBounds(293, 384, 116, 22);
		frame.getContentPane().add(textField_21);
		
		textField_22 = new JTextField();
		textField_22.setColumns(10);
		textField_22.setBounds(12, 419, 116, 22);
		frame.getContentPane().add(textField_22);
		
		textField_23 = new JTextField();
		textField_23.setColumns(10);
		textField_23.setBounds(157, 419, 116, 22);
		frame.getContentPane().add(textField_23);
		
		textField_24 = new JTextField();
		textField_24.setColumns(10);
		textField_24.setBounds(293, 419, 116, 22);
		frame.getContentPane().add(textField_24);
		
		textField_25 = new JTextField();
		textField_25.setColumns(10);
		textField_25.setBounds(12, 454, 116, 22);
		frame.getContentPane().add(textField_25);
		
		textField_26 = new JTextField();
		textField_26.setColumns(10);
		textField_26.setBounds(157, 454, 116, 22);
		frame.getContentPane().add(textField_26);
		
		textField_27 = new JTextField();
		textField_27.setColumns(10);
		textField_27.setBounds(293, 454, 116, 22);
		frame.getContentPane().add(textField_27);
		
		textField_28 = new JTextField();
		textField_28.setColumns(10);
		textField_28.setBounds(12, 489, 116, 22);
		frame.getContentPane().add(textField_28);
		
		textField_29 = new JTextField();
		textField_29.setColumns(10);
		textField_29.setBounds(157, 489, 116, 22);
		frame.getContentPane().add(textField_29);
		
		textField_30 = new JTextField();
		textField_30.setColumns(10);
		textField_30.setBounds(293, 489, 116, 22);
		frame.getContentPane().add(textField_30);
		
		textField_31 = new JTextField();
		textField_31.setColumns(10);
		textField_31.setBounds(12, 524, 116, 22);
		frame.getContentPane().add(textField_31);
		
		textField_32 = new JTextField();
		textField_32.setColumns(10);
		textField_32.setBounds(157, 524, 116, 22);
		frame.getContentPane().add(textField_32);
		
		textField_33 = new JTextField();
		textField_33.setColumns(10);
		textField_33.setBounds(293, 524, 116, 22);
		frame.getContentPane().add(textField_33);
		
		separator_1 = new JSeparator();
		separator_1.setBounds(12, 40, 397, 2);
		frame.getContentPane().add(separator_1);
	}
}
