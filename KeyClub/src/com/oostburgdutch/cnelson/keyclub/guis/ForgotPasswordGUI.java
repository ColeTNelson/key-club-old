package com.oostburgdutch.cnelson.keyclub.guis;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.JButton;

public class ForgotPasswordGUI {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ForgotPasswordGUI window = new ForgotPasswordGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ForgotPasswordGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 312, 245);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Key Club - Forgot Password");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 273, 29);
		frame.getContentPane().add(lblKeyClub);
		
		textField = new JTextField();
		textField.setBounds(75, 55, 198, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Email");
		lblNewLabel.setBounds(22, 58, 41, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JTextPane txtpnAn = new JTextPane();
		txtpnAn.setBackground(SystemColor.control);
		txtpnAn.setText("An email will be sent to you containing your new password. Make sure to reset this as soon as you login.");
		txtpnAn.setBounds(32, 90, 250, 57);
		frame.getContentPane().add(txtpnAn);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(117, 160, 80, 25);
		frame.getContentPane().add(btnSubmit);
	}
}
