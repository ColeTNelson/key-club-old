package com.oostburgdutch.cnelson.keyclub.guis;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JSeparator;

public class ReminderEmailGUI {

	private JFrame frmKeyClub;
	private JPasswordField passwordField;
	private JTextField txtOostburgkeyclubgmailcom;
	private JSeparator separator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				
			}
		});*/
		try {
			ReminderEmailGUI window = new ReminderEmailGUI();
			window.frmKeyClub.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the application.
	 */
	public ReminderEmailGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKeyClub = new JFrame();
		frmKeyClub.setTitle("Key Club - Reminder Email");
		frmKeyClub.setBounds(100, 100, 450, 300);
		frmKeyClub.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKeyClub.getContentPane().setLayout(null);
		
		passwordField = new JPasswordField();
		passwordField.setToolTipText("Enter your password here...");
		passwordField.setBounds(214, 44, 122, 22);
		frmKeyClub.getContentPane().add(passwordField);
		
		JTextArea textArea = new JTextArea();
		textArea.setToolTipText("Enter your message here...");
		textArea.setBounds(12, 79, 408, 161);
		frmKeyClub.getContentPane().add(textArea);
		
		txtOostburgkeyclubgmailcom = new JTextField();
		txtOostburgkeyclubgmailcom.setToolTipText("Enter your gmail here...");
		txtOostburgkeyclubgmailcom.setText("oostburgkeyclub@gmail.com");
		txtOostburgkeyclubgmailcom.setBounds(12, 44, 190, 23);
		frmKeyClub.getContentPane().add(txtOostburgkeyclubgmailcom);
		txtOostburgkeyclubgmailcom.setColumns(10);
		
		JButton btnSend = new JButton("Send");
		btnSend.setBounds(348, 43, 72, 25);
		frmKeyClub.getContentPane().add(btnSend);
		
		JLabel lblCustomReminderEmail = new JLabel("Key Club - Send Reminder E-Mail");
		lblCustomReminderEmail.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCustomReminderEmail.setBounds(12, 7, 408, 23);
		frmKeyClub.getContentPane().add(lblCustomReminderEmail);
		
		separator = new JSeparator();
		separator.setBounds(12, 32, 408, 2);
		frmKeyClub.getContentPane().add(separator);
	}
}
