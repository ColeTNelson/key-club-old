package com.oostburgdutch.cnelson.keyclub.guis;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JEditorPane;
import javax.swing.JSlider;
import javax.swing.JProgressBar;
import javax.swing.JList;

public class MenuGUI {

	private JFrame frmKeyClub;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuGUI window = new MenuGUI();
					window.frmKeyClub.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKeyClub = new JFrame();
		frmKeyClub.setTitle("Key Club - Menu");
		frmKeyClub.setBounds(100, 100, 450, 300);
		frmKeyClub.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKeyClub.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Key Club - Menu");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 219, 30);
		frmKeyClub.getContentPane().add(lblKeyClub);
		
		JButton btnEvents = new JButton("Events");
		btnEvents.setBounds(12, 78, 97, 25);
		frmKeyClub.getContentPane().add(btnEvents);
		
		JButton btnProfile = new JButton("Profile");
		btnProfile.setBounds(162, 17, 80, 25);
		frmKeyClub.getContentPane().add(btnProfile);
		
		JButton btnNotifications = new JButton("Notifications");
		btnNotifications.setBounds(254, 17, 124, 25);
		frmKeyClub.getContentPane().add(btnNotifications);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 62, 408, 2);
		frmKeyClub.getContentPane().add(separator);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setBounds(254, 104, 166, 136);
		frmKeyClub.getContentPane().add(editorPane);
		
		JButton btnCalendar = new JButton("Calendar");
		btnCalendar.setBounds(145, 163, 97, 25);
		frmKeyClub.getContentPane().add(btnCalendar);
		
		JButton btnWebsite = new JButton("Website");
		btnWebsite.setBounds(12, 201, 97, 25);
		frmKeyClub.getContentPane().add(btnWebsite);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 154, 232, 2);
		frmKeyClub.getContentPane().add(separator_1);
		
		JLabel lblChatbox = new JLabel("Updates");
		lblChatbox.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblChatbox.setBounds(298, 81, 80, 16);
		frmKeyClub.getContentPane().add(lblChatbox);
		
		JButton btnAdmin = new JButton("Administrative");
		btnAdmin.setBounds(22, 116, 209, 25);
		frmKeyClub.getContentPane().add(btnAdmin);
		
		JButton btnHours = new JButton("Hours");
		btnHours.setBounds(145, 78, 97, 25);
		frmKeyClub.getContentPane().add(btnHours);
		
		JButton btnPolicy = new JButton("Policy");
		btnPolicy.setBounds(12, 163, 97, 25);
		frmKeyClub.getContentPane().add(btnPolicy);
		
		JButton btnContact = new JButton("Contact");
		btnContact.setBounds(145, 201, 97, 25);
		frmKeyClub.getContentPane().add(btnContact);
	}
}
