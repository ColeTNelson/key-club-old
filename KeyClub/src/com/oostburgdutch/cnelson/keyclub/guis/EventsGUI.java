package com.oostburgdutch.cnelson.keyclub.guis;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.SystemColor;

public class EventsGUI {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EventsGUI window = new EventsGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EventsGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 571);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JList list = new JList();
		list.setBounds(12, 102, 287, 168);
		frame.getContentPane().add(list);
		
		JLabel lblKeyClub = new JLabel("Key Club - Events");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 149, 29);
		frame.getContentPane().add(lblKeyClub);
		
		JButton btnSeeArchivedEvents = new JButton("Archived Events");
		btnSeeArchivedEvents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSeeArchivedEvents.setBounds(173, 16, 134, 25);
		frame.getContentPane().add(btnSeeArchivedEvents);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 55, 408, 2);
		frame.getContentPane().add(separator);
		
		JLabel lblCurrentEvents = new JLabel("Current Events Listing");
		lblCurrentEvents.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCurrentEvents.setBounds(65, 64, 169, 25);
		frame.getContentPane().add(lblCurrentEvents);
		
		JButton btnView = new JButton("View");
		btnView.setBounds(327, 109, 76, 25);
		frame.getContentPane().add(btnView);
		
		JButton btnSignup = new JButton("Sign Me Up\r\n");
		btnSignup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSignup.setBounds(310, 147, 109, 25);
		frame.getContentPane().add(btnSignup);
		
		JButton btnRemove = new JButton("Remove Me");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRemove.setBounds(310, 185, 109, 25);
		frame.getContentPane().add(btnRemove);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(12, 283, 408, 2);
		frame.getContentPane().add(separator_1);
		
		JButton btnModify = new JButton("Modify");
		btnModify.setBounds(327, 223, 81, 25);
		frame.getContentPane().add(btnModify);
		
		JLabel lblOngoingEventsListing = new JLabel("Ongoing Events Listing");
		lblOngoingEventsListing.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblOngoingEventsListing.setBounds(65, 298, 169, 25);
		frame.getContentPane().add(lblOngoingEventsListing);
		
		JList list_1 = new JList();
		list_1.setBounds(12, 336, 287, 168);
		frame.getContentPane().add(list_1);
		
		JButton button = new JButton("View");
		button.setBounds(327, 343, 76, 25);
		frame.getContentPane().add(button);
		
		JButton button_1 = new JButton("Sign Me Up\r\n");
		button_1.setBounds(310, 381, 109, 25);
		frame.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("Remove Me");
		button_2.setBounds(310, 419, 109, 25);
		frame.getContentPane().add(button_2);
		
		JButton button_3 = new JButton("Modify");
		button_3.setBounds(327, 457, 81, 25);
		frame.getContentPane().add(button_3);
	}

}
