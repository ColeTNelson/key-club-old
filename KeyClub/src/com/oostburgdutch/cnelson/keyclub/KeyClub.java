package com.oostburgdutch.cnelson.keyclub;

import com.oostburgdutch.cnelson.keyclub.commands.CommandCustomReminder;
import com.oostburgdutch.cnelson.keyclub.commands.CommandReload;
import com.oostburgdutch.cnelson.keyclub.commands.CommandReminder;
import com.oostburgdutch.cnelson.keyclub.commands.CommandSearch;
import com.oostburgdutch.cnelson.keyclub.guis.ReminderEmailGUI;

public class KeyClub {
	
	public static void main(String[] args){                  
		Database.init();
		KeyClub.splash(true);
	}
	
	public static void splash(boolean splash){
		if(splash){
			System.out.println();
			System.out.println(" == OOSTBURG KEY CLUB - TECHNOLOGY PANEL == ");
			System.out.println("      --------------------------------      ");
			System.out.println("   Last Updated: 1/5/2015 - Cole Nelson    ");
			System.out.println();
			System.out.println(" [1] Search for member");
			System.out.println(" [2] Send reminder e-mail");
			System.out.println(" [3] Send custom e-mail");
			System.out.println(" [0] Reload database");
		}
		System.out.println();
		System.out.println(" Command: ");
		String choice = Util.getScanner().nextLine();
		if(choice.equalsIgnoreCase("1") || choice.equalsIgnoreCase("search")){
			CommandSearch.execute(true);
			return;
		}else if(choice.equalsIgnoreCase("2") || choice.equalsIgnoreCase("reminder")){
			CommandReminder.execute(true);
			return;
		}else if(choice.equalsIgnoreCase("3") || choice.equalsIgnoreCase("custom")){
			CommandCustomReminder.execute(true);
			return;
		}else if(choice.equalsIgnoreCase("0") || choice.equalsIgnoreCase("reload")){
			CommandReload.execute();
			return;
		}
		System.out.println();
		System.out.println("Invalid command!");
		KeyClub.splash(false);
	}
}
