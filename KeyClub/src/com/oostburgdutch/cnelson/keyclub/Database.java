package com.oostburgdutch.cnelson.keyclub;

import java.util.ArrayList;

import com.oostburgdutch.cnelson.keyclub.enums.Grade;
import com.oostburgdutch.cnelson.keyclub.enums.Role;
import com.oostburgdutch.cnelson.keyclub.objects.CSV;
import com.oostburgdutch.cnelson.keyclub.objects.Member;

public class Database {
	
	private static String flMembers = "C:\\Users\\Cole Nelson\\Desktop\\Key Club\\Members.csv";
	private static ArrayList<Member> members = new ArrayList<Member>();
	
	public static void init(){
		try {
			System.out.println(Encrypter.generatePassword());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initMembers();
	}
	
	private static void initMembers(){
		members.clear();
		CSV csv;
		try {
			csv = new CSV(flMembers);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		for(int i=1;i<csv.getTotalRows()+1;i++){
			ArrayList<String> row = csv.getRow(i);
			row.trimToSize();
			if(row.size()>5){
				String fName = row.get(0);
				String lName = row.get(1);
				String email = row.get(3);
				Grade grade;
				String role = row.get(5);
				String password = "thereisnourflevel";
				try {
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				if(row.size()>6 && row.get(6)!=null){
					try {
						password = Encrypter.decrypt(row.get(6));
						System.out.println(password);
					} catch (Exception e1){}
				}
				try{
					Integer.parseInt(row.get(4));
					grade = Grade.valueOfNumber(row.get(4));
				}catch(NumberFormatException e){
					grade = Grade.valueOf(row.get(4).toUpperCase());
				}
				if(row.get(2).isEmpty()){
					members.add(new Member(fName, lName, email, grade, Role.valueOf(role.toUpperCase()), password));
				}else{
					members.add(new Member(fName, lName, row.get(2), email, grade, Role.valueOf(role.toUpperCase()), password));
				}
			}else{
				System.out.println("Error: Unknown row size on row "+i+". Skipping...");
			}
		}
	}

	public static ArrayList<Member> getMembers() {
		return members;
	}
}
