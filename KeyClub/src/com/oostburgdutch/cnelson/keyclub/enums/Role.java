package com.oostburgdutch.cnelson.keyclub.enums;

public enum Role {
	ADVISOR,
	OFFICER,
	MEMBER
}
